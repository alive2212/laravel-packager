<?php

namespace App\Providers;

use Alive2212\LaravelMobilePassport\AliveMobilePassportDevice;
use Alive2212\LaravelMobilePassport\LaravelMobilePassport;
use Alive2212\LaravelMobilePassport\LaravelMobilePassportSingleton;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::routes();

        LaravelMobilePassport::initPassportTokenCan();

        LaravelMobilePassportSingleton::$otpCallBack = function (
            Request $reqeust,
            User $user,
            AliveMobilePassportDevice $device,
            $token
        ) {
            Log::info($token);
            // dispach send sms job here to send notification
        };


        LaravelMobilePassportSingleton::$otpConfirmCallBack = function (
            Request $reqeust,
            User $user
        ) {
            // put somthing here like update user name with request fileds
        };    }
}
