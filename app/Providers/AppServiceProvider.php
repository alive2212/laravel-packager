<?php

namespace App\Providers;

use Alive2212\LaravelMobilePassport\LaravelMobilePassport;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        LaravelMobilePassport::routes(null,['middleware'=>'cors']);
    }
}
